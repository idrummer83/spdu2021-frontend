# Variant 3
Develop a traffic light signs using HTML and CSS. <br />
Save your solution to a file **ui-task-v3.html** and commit to the **master** branch.

**Tips**: You can use https://jsfiddle.net/ for solving this task.

<img src="/img/web-ui-task3.png" width="350" />