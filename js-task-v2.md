# Variant 2
Given an array of numbers, write a function that will return true if every array item is greater than the previous one. <br />
Save your solution to a file **js-task-v2.js** and commit to the **master** branch.

`console.log(nums([1, 2, 3, 4, 5, 6]));` <br />
`true`

`console.log(nums([1, 3, 2, 4, 5, 6]));` <br />
`false`

**Tips**: You can use https://jsfiddle.net/ for solving this task.